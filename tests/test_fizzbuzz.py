from coe_number.Fizzbuzz import fizz_buzz
import unittest

class fizztest(unittest.TestCase):
 def test_give_15_to_fizz(self):
    fizz_list = 15
    is_fizz = fizz_buzz(fizz_list)
    self.assertTrue(is_fizz)

 def test_give_0_to_fizz(self):
    fizz_list = 0
    is_fizz = fizz_buzz(fizz_list)
    self.assertTrue(is_fizz)

 def test_give_5_to_fizz(self):
    fizz_list = 5
    is_fizz = fizz_buzz(fizz_list)
    self.assertTrue(is_fizz)

 def test_give_7_to_fizz(self):
    fizz_list = 7
    is_fizz = fizz_buzz(fizz_list)
    self.assertTrue(is_fizz)

 def test_give_4dot2_to_fizz(self):
    fizz_list = 4.2
    is_fizz = fizz_buzz(fizz_list)
    self.assertTrue(is_fizz)