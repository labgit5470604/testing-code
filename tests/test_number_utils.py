from coe_number.number_utils import is_prime_list
import unittest

class PrimeListTest(unittest.TestCase):
 def test_give_1_2_3_is_prime(self):
    prime_list = [1, 2, 3]
    is_prime = is_prime_list(prime_list)
    self.assertTrue(is_prime)

 def test_give_7_8_3_is_prime(self):
    prime_list = [7, 8, 3]
    is_prime = is_prime_list(prime_list)
    self.assertTrue(is_prime)

 def test_give_2_9_4_is_prime(self):
    prime_list = [-2, -9, -4]
    is_prime = is_prime_list(prime_list)
    self.assertTrue(is_prime)

 def test_give_5_6_1_is_prime(self):
    prime_list = [5, -6, 1]
    is_prime = is_prime_list(prime_list)
    self.assertTrue(is_prime)

 def test_give_9_3_3_is_prime(self):
    prime_list = [9, 3, 3]
    is_prime = is_prime_list(prime_list)
    self.assertTrue(is_prime)

