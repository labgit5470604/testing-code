from coe_number.cat_mouse import cat_and_mouse
import unittest

class distance_cat_mouse_Test(unittest.TestCase):
 def test_give_distance_cat_mouse(self):
    cat_a = 2
    cat_b = 3 
    mouse = 5
    cat_win = cat_and_mouse(cat_a,cat_b,mouse)
    self.assertTrue(cat_win)

 def test_give_distance2_cat_mouse(self):
    cat_a = 10
    cat_b = 5 
    mouse = 7
    cat_win = cat_and_mouse(cat_a,cat_b,mouse)
    self.assertTrue(cat_win)

 def test_give_distance3_cat_mouse(self):
    cat_a = 8
    cat_b = 7 
    mouse = 6.1
    cat_win = cat_and_mouse(cat_a,cat_b,mouse)
    self.assertTrue(cat_win)

 def test_give_distance4_cat_mouse(self):
    cat_a = -4
    cat_b = 1 
    mouse = 9
    cat_win = cat_and_mouse(cat_a,cat_b,mouse)
    self.assertTrue(cat_win)

 def test_give_distance5_cat_mouse(self):
    cat_a = -2
    cat_b = -3 
    mouse = -5
    cat_win = cat_and_mouse(cat_a,cat_b,mouse)
    self.assertTrue(cat_win)

